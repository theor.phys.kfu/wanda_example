#include <iostream>
#include <random>
#include <chrono>

#include <specmath/wanda.h>

#include <boost/gil.hpp>
#include <boost/gil/extension/io/png.hpp>


int main()
{
    boost::gil::rgb8_image_t img;
    boost::gil::read_image("../photo/photo.png", img, boost::gil::png_tag());
    boost::gil::rgb8_view_t view = boost::gil::view(img);

    const size_t TRAINING_SIZE = view.size()/20; // 5% pixels
    specmath::compsci::training_dataset dataset;
    dataset.reserve(TRAINING_SIZE);

    std::random_device rd;
    std::mt19937 gen(rd());

    std::uniform_int_distribution<size_t> dist_x(0, view.width()-1);
    std::uniform_int_distribution<size_t> dist_y(0, view.height()-1);


    auto start = std::chrono::high_resolution_clock::now();

    for (size_t i = 0; i < TRAINING_SIZE; ++i)
    {
        size_t xi = dist_x(gen);
        size_t yi = dist_y(gen);

        boost::gil::rgb8_pixel_t pixel = view(xi, yi);

        dataset.add( { double(xi), double(yi) },
                     { double(pixel[0]), double(pixel[1]), double(pixel[2]) } );
    }

    specmath::compsci::wanda wanda(dataset);

    auto stop = std::chrono::high_resolution_clock::now();

    auto diff = std::chrono::duration_cast<std::chrono::seconds>(stop - start).count();

    std::cout << "Training time (sec) = "  <<  diff << std::endl;
    std::cout << "TOTAL SIZE = " << view.size() << std::endl;
    std::cout << "TRAINING SIZE = " << TRAINING_SIZE << std::endl;

    start = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < view.width(); ++i)
    {
        for (int j = 0; j < view.height(); ++j)
        {
            specmath::compsci::vec_t xv = { double(i), double(j) };
            specmath::compsci::vec_t res = wanda.predict(xv, 25);

            view(i, j) = boost::gil::rgb8_pixel_t( res[0], res[1], res[2] );
        }
    }

    stop = std::chrono::high_resolution_clock::now();

    diff = std::chrono::duration_cast<std::chrono::minutes>(stop - start).count();

    std::cout << "Total duration (min) = "  <<  diff << std::endl;

    boost::gil::write_view("restore_photo.png", view, boost::gil::png_tag());

    return 0;
}
